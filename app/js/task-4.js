let form = document.getElementsByClassName('hashtag-maker')[0];
// one line solution
let makeHashTag = (text)=>{
    return `#${text.toLowerCase().replace(/[^0-9a-z\s]/gi, '').split(' ').map((item)=>item.charAt(0).toUpperCase()+item.slice(1)).join('')}`
}

// understandable solution, step-by-step

let createHashTag = (text) => {
    let alphaNumerical = text.toLowerCase().replace(/[^0-9a-z\s]/gi, '');
    let words = alphaNumerical.split(' ');
    let capitalize = words.map((item)=>{
        return item.charAt(0).toUpperCase()+item.slice(1);
    });
    return `#${capitalize.join('')}`;
}

form.addEventListener('submit',(event)=>{

    event.preventDefault();
    let text = form.querySelector('input[type=text]');
    console.log(makeHashTag(text.value));
    console.log(createHashTag(text.value));
    document.querySelector('.result').innerHTML = makeHashTag(text.value)
})