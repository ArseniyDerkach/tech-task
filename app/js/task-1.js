let buttonOverlay = document.getElementsByClassName('button-with-tooltip')[0];
let button = buttonOverlay.getElementsByClassName('btn')[0];
let tooltip = buttonOverlay.getElementsByClassName('tooltip')[0];
let closeButton = button.getElementsByClassName('btn-close')[0];

button.addEventListener('mouseenter',(event)=>{
    tooltip.style.display="";
}, true);
button.addEventListener('mouseleave',(event)=>{
        tooltip.style.display="block";
});
button.addEventListener('click',(event)=>{
    alert('hello world');
})
closeButton.addEventListener('click',(event)=>{
    buttonOverlay.style.display='none';
    event.stopPropagation();
})