openTabContent = (theme)=> {
    let tabs = document.getElementsByClassName('tab-content');
    for(let i = 0;i<tabs.length;i++) {
        if (tabs[i].dataset.theme == theme) {
            tabs[i].classList.add('active');
        } else {
            tabs[i].classList.remove('active');
        }
    }
}

openTabContent('dashboard');

document.querySelector('.main-nav').addEventListener('click',(event)=>{
    event.preventDefault();
    if (event.target.classList.contains('main-nav-item-link')){
        let item = event.target.closest('.main-nav-item.has-subnav');
        if (item) {
            if (!item.classList.contains('active')){
                let navItems = document.getElementsByClassName('main-nav-item');
                for (let i=0;i<navItems.length;i++){
                    navItems[i].classList.remove('active');
                }
            }
            event.target.closest('.main-nav-item').classList.toggle('active');
        }
    }
    if (event.target.dataset.toggle) {
        openTabContent(event.target.dataset.toggle);
    }
})